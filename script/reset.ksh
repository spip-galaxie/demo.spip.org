#!/bin/bash

## Install de demo : 
# cd /home/sites_manuel/spip.net/demo/ 
# mv public_html public_html_old
# checkout -bmaster public_html 
# chown www-data:www-data
REP=/home/sites_manuel/spip.net/demo/public_html
GIT=/home/sites_manuel/spip.net/demo/git
USER=www-data
GROUP=www-data

if [ -f "mes_options.inc" ]; then source "mes_options.inc"; fi

cd $GIT
git stash 
git pull 
git stash pop 


#on fait un git pull sur la branche actuelle 
cd $REP 
git checkout -- . 
checkout spip -b4.4 . 


echo "--- Les plugins dist" 
mkdir -p $REP/plugins-dist  
cd $REP/plugins-dist 
if [ ! -d nospam ]; then
       git clone https://git.spip.net/spip-contrib-extensions/nospam.git nospam
fi
cd $REP/plugins-dist/nospam 
git pull 

#Le mes_options entre dans la place 
echo "<?php" > $REP/config/mes_options.php
echo "define('_SPAM_ENCRYPT_NAME', true);" >> $REP/config/mes_options.php
# echo "define('_DEV_VERSION_SPIP_COMPAT', '4.2.99');" >> $REP/config/mes_options.php

#on vide les cache 
rm -rf $REP/tmp/* 
rm -rf $REP/tmp/cache 
rm -rf $REP/local/*
rm -rf $REP/IMG/*

#on recopie les images et la base 
echo "cp -p -r $GIT/IMG/* $REP/IMG/" 
cp -p -r $GIT/IMG/* $REP/IMG/

echo "$GIT/config/bases/spip.sqlite $REP/config/bases/spip.sqlite" 
cp $GIT/config/bases/spip.sqlite $REP/config/bases/spip.sqlite 
chmod 755 $REP/config/bases/spip.sqlite 

chown -R $USER:$GROUP $REP
 
